package NGuessNumber;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
public class NGuessNumber {
    private int[] nums = new int[4];
    public String toString() {
        StringBuffer result = new StringBuffer("");
        for (int i : nums) {
            result.append(i + " ");
        }
        return result.toString();
    }
    public NGuessNumber() {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        Random r = new Random();
        for (int i = 0; i < 4; i++) {
            int k = r.nextInt(10 - i);
            nums[i] = list.get(k);
            list.remove(k);
        }
    }
    public boolean validate(String r) {
        if (!r.matches("\\d{4}")) {
            return false;
        }
        for (int i = 0; i < 4; i++) {
            if (r.lastIndexOf(r.charAt(i)) > i) {
                return false;
            }
        }
        return true;
    }
    private String check1(String r) {
        int count = 0;
        for (int i = 0; i < r.length(); i++) {
            if (nums[i] == Integer.parseInt(r.charAt(i) + "")) {
                count++;
            }
        }
        return count + "A";
    }
    private String check2(String r) {
        int count = 0;
        for (int i = 0; i < r.length(); i++) {
            int temp = Integer.parseInt(r.charAt(i) + "");
            for (int j = 0; j < r.length(); j++) {
                if (nums[j] == temp && i != j)
                    count++;
            }
        }
        return count + "B";
    }
    public String check(String r) {
        return check1(r) + check2(r);
    }
    public static void main(String[] args) {
        NGuessNumber g = new NGuessNumber();
        System.out.println("请输入4个0-9之间的数字");
        Scanner s = new Scanner(System.in);
        yy:
        while (true) {
            while (true) {
                String r = s.next();
                if ("exit".equals(r))
                    break;
                if (!g.validate(r)) {
                    System.out.println("输入正确的4位数字, 且不能重复");
                    continue;
                }
                String result = g.check(r);
                System.out.println("A表示存在且位置正确的数字个数，B表示存在但位置不正确的数字个数");
                System.out.println("如：1A2B，表示你猜测的四个数字中有三个是对的，1个位置对，2个位置不对");
                if ("4A0B".equals(result)) {
                    System.out.println("恭喜, 猜对了");
                    System.out.println("请问是否要再进行一局?（ y 继续、n 或其他退出）");
                    Scanner sca = new Scanner(System.in);
                    if (sca.hasNext("yes")) {
                        System.out.println("重新开始请输入");
                        g = new NGuessNumber();
                    } else {
                        System.out.println("游戏结束");
                        break yy;
                    }
                    break;
                } else {
                    System.out.println(result);

                }
            }

        }
    }
}
